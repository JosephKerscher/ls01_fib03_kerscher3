import java.util.Scanner;

class Fahrkartenautomat
{
	
	 static Scanner tastatur = new Scanner(System.in);
	
	public static double fahrkartenErstellungErfassen(double zuZahlenderBetrag) {
		
		
		

		System.out.println("Wie viele Tickets m�chten Sie kaufen: ");
	     int ticketAnzahl = tastatur.nextInt();
	      // System.out.print("Zu zahlender Betrag (EURO): ");
	      // zuZahlenderBetrag = tastatur.nextDouble();
	       zuZahlenderBetrag = ticketAnzahl * zuZahlenderBetrag;
		
		return zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, double eingezahlterGesamtbetrag){
		//System.out.println("dddddd");
		 eingezahlterGesamtbetrag = 0.00;
		double eingeworfeneM�nze;
	       
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   
	    	   System.out.printf("Noch %.2f� zu zahlen: ",  zuZahlenderBetrag - eingezahlterGesamtbetrag);
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
		
		
		return eingezahlterGesamtbetrag;
		
	}
	
	public static void fahrkartenAusgeben() {
	       System.out.println("\nFahrschein wird ausgegeben");
	      /*
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }*/
	       System.out.println("\n\n");
	       
	    }
	
	public static void warten (int millisekunden) {
		
		for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       }
		System.out.println("\n\n");
	}
	
	public static double rueckgeldAusgeben(double r�ckgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag){
		
		 r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.00)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f� " , r�ckgabebetrag );
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.00;
		          r�ckgabebetrag= Math.round(r�ckgabebetrag *100.0) /100.0 ;
	           }
	           while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 Euro");
		          r�ckgabebetrag -= 1.00;
		          r�ckgabebetrag= Math.round(r�ckgabebetrag *100.0) /100.0 ;
	           }
	           while(r�ckgabebetrag >= 0.50) // 50 Cent-M�nzen
	           {
	        	  System.out.println("50 Cent");
		          r�ckgabebetrag -= 0.50;
		          r�ckgabebetrag= Math.round(r�ckgabebetrag *100.0) /100.0 ;
	           }
	           while(r�ckgabebetrag >= 0.20) // 20 Cent-M�nzen
	           {
	        	  System.out.println("20 Cent");
	 	          r�ckgabebetrag -= 0.20;
	 	          r�ckgabebetrag= Math.round(r�ckgabebetrag *100.0) /100.0 ;
	           }
	           while(r�ckgabebetrag >= 0.10) // 10 Cent-M�nzen
	           {
	        	  System.out.println("10 Cent");
		          r�ckgabebetrag -= 0.10;
		          r�ckgabebetrag= Math.round(r�ckgabebetrag *100.0) /100.0 ;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 Cent-M�nzen
	           {
	        	  System.out.println("5 Cent");
	 	          r�ckgabebetrag -= 0.05;
	 	          r�ckgabebetrag= Math.round(r�ckgabebetrag *100.00) /100.00 ;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
		
		return r�ckgabebetrag;
	}
	
    public static void main(String[] args)
    {
      
        double eingezahlterGesamtbetrag =0;
        double zuZahlenderBetrag = 0; 
       // double eingezahlterGesamtbetrag;
        //double eingeworfeneM�nze;
        double r�ckgabebetrag = 0;
        double einzelpreis = 1.70;
        double tageskarte = 3.00;
        double gruppenkarte = 8.50;
        
    	
    	while(true) {
    		
    		System.out.println("0. exit");
    		System.out.println("1. Einzelfahrscheine		(st. 1,70�)");
    		System.out.println("2. Tageskarte		 	(st. 3,10�)");
    		System.out.println("3. Gruppenkarte		 	(st. 8,50�)");
    		
    		
    		int intSwitch = tastatur.nextInt();
    		
    	
    		
    		switch(intSwitch) {
    		
    			case 0:
    				System.out.println("Auf Wiedersehen");
    				System.exit(0);
    				
    			case 1:
    				zuZahlenderBetrag = zuZahlenderBetrag + einzelpreis;
    				break;
    				
    			case 2:
    				zuZahlenderBetrag = zuZahlenderBetrag + tageskarte;
    				break;
    			
    			case 3:
    				zuZahlenderBetrag = zuZahlenderBetrag + gruppenkarte;
    				break;	
    		
    			 default:
    		            System.out.println("Nicht zur Auswahl");
    		            
    		}
    	
  
       
     /*  public static String format(double i) {
       	DecimalFormat f = new DecimalFormat("#0.00");
       	double toFormat = ((double)Math.round(i*100))/100;
       	return f.format(toFormat);
       }
*/
       
       zuZahlenderBetrag = fahrkartenErstellungErfassen(zuZahlenderBetrag);       
       System.out.println(zuZahlenderBetrag);
       
       // Geldeinwurf
       // -----------
      
      eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag,eingezahlterGesamtbetrag);
      System.out.println(eingezahlterGesamtbetrag);
       
       // Fahrscheinausgabe
       // -----------------
      fahrkartenAusgeben();
      
      //Warten
      //-------------------
      warten(300);
      
       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
      r�ckgabebetrag = rueckgeldAusgeben(r�ckgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag);
      System.out.println("\n");
    	}
    }
}